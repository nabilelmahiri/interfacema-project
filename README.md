########## InterfaceMaProject ##########

## run app

Run `ng serve --proxy-config proxy.conf.json`, to avoid crossorigin error when calling the Api resource from local

## Documentation

Install cocompodoc : `npm install -g @compodoc/compodoc`
Run documentation using this command : `compodoc -s -r 8181`
Regenarate documentation : `compodoc -p tsconfig.json -d 'documentation'`

## Running unit tests

Run `ng test` to execute the unit tests via [Karma].
Run `ng test --code-coverage` for code-coverage report

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.
