import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { Routes, RouterModule } from "@angular/router";
import { HousesComponent } from "./houses/houses.component";

const routes: Routes = [
  {
    path: "",
    component: HousesComponent,
    data: { title: "Houses" }
  }
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(
      routes,
      {
        // anchorScrolling: "enabled",
        // scrollPositionRestoration: "enabled",
        //  useHash: true,
        //  enableTracing: false
      } // <-- debugging purposes only
    )
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
