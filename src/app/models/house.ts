export interface House {
  coords: Coords;
  params?: Params;
  street: string;
}

export interface Coords {
  lat: number;
  lon: number;
}

export interface Params {
  rooms?: number;
  value?: number;
}
