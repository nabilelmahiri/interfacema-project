import { async, TestBed, tick } from "@angular/core/testing";
import { FormsModule } from "@angular/forms";
import { MaterialModule } from "../material/material.module";
import { HousesComponent } from "./houses.component";
import { HouseApiService } from "../services/houses-api.service";
import { HttpClientTestingModule } from "@angular/common/http/testing";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { houses } from "../services/data";
import { House } from "../models/house";
import { of } from "rxjs";
import { By } from "@angular/platform-browser";
describe("HousesComponent", () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [HousesComponent],
      imports: [
        MaterialModule,
        FormsModule,
        HttpClientTestingModule,
        BrowserAnimationsModule
      ],
      providers: [HouseApiService]
    }).compileComponents();
  }));

  function setup() {
    const fixture = TestBed.createComponent(HousesComponent);
    const app = fixture.debugElement.componentInstance;
    const service: HouseApiService = TestBed.get(HouseApiService);
    return { fixture, service, app };
  }

  it('should call change method on slide change "toggleRooms"', () => {
    const { fixture, app } = setup();
    const compile = fixture.debugElement.nativeElement;

    const slider = fixture.debugElement.query(de => {
      return de.nativeElement.id === "toggleRooms";
    });

    spyOn(app, "toggle");

    slider.triggerEventHandler("change", { checked: false });

    expect(app.toggle).toHaveBeenCalled(); // event has been called
  });

  it('should call change method on slide change "toggleHouses"', () => {
    const { fixture, app } = setup();
    const compile = fixture.debugElement.nativeElement;

    const slider = fixture.debugElement.query(de => {
      return de.nativeElement.id === "toggleHouses";
    });

    spyOn(app, "toggle");

    slider.triggerEventHandler("change", { checked: false });

    expect(app.toggle).toHaveBeenCalled(); // event has been called
  });

  it('should call change method on slide change "toggleData"', () => {
    const { fixture, app } = setup();
    const compile = fixture.debugElement.nativeElement;

    const slider = fixture.debugElement.query(de => {
      return de.nativeElement.id === "toggleData";
    });

    spyOn(app, "toggle");

    slider.triggerEventHandler("change", { checked: false });

    expect(app.toggle).toHaveBeenCalled(); // event has been called
  });

  it('should call change method on slide change "toggleMove"', () => {
    const { fixture, app } = setup();
    const compile = fixture.debugElement.nativeElement;

    const slider = fixture.debugElement.query(de => {
      return de.nativeElement.id === "toggleMove";
    });

    spyOn(app, "toggle");

    slider.triggerEventHandler("change", { checked: false });

    expect(app.toggle).toHaveBeenCalled(); // event has been called
  });

  it("should get distance Between Points", () => {
    const { app } = setup();
    expect(app.sisHomeCoords).toBeTruthy([13.4057378, 52.5418739]);
  });

  it("should set sisHomeCoords", () => {
    const { app } = setup();
    expect(
      app.distanceBetweenPoints(
        [13.4174913, 52.5013632],
        [13.4057378, 52.5418739]
      )
    ).toBeTruthy(12.5);
  });

  it("should get data from api service", () => {
    const { service, fixture } = setup();
    console.log(houses);
    const mockInterfaceMaData: House[] = houses;
    spyOn(service, "getHouses").and.returnValue(of(mockInterfaceMaData));

    fixture.detectChanges();
    const compile = fixture.debugElement.nativeElement;
    const loadingData = compile.querySelector("mat-cell");
    expect(loadingData.textContent).toBe("7.5 Km");
  });

  it("should test the table ", done => {
    const { fixture, app } = setup();

    fixture.detectChanges();
    fixture.whenStable().then(() => {
      fixture.detectChanges();

      const tableRows = fixture.nativeElement.querySelectorAll(
        "mat-header-cell"
      );
      expect(tableRows.length).toBe(4);

      // Header row
      const headerRow = tableRows[0];
      expect(headerRow.cells[0].innerHTML).toBe("Location");
      expect(headerRow.cells[1].innerHTML).toBe("Rooms");
      expect(headerRow.cells[2].innerHTML).toBe("Value");
      expect(headerRow.cells[3].innerHTML).toBe("Street");

      // Data rows
      const row1 = tableRows[1];
      expect(row1.cells[0].innerHTML).toBe("7.5 km");
      expect(row1.cells[1].innerHTML).toBe("5");
      expect(row1.cells[2].innerHTML).toBe("1000000 $");
      expect(row1.cells[3].innerHTML).toBe("Adalbertstraße 13");
    });
    done();
  });

  it("should create the HousesComponent", () => {
    const { app } = setup();
    expect(app).toBeTruthy();
  });
});
