import { Component, OnInit, ViewChild, Input } from "@angular/core";
import {
  MatTableDataSource,
  MatSort,
  MatPaginator,
  MatSlideToggleChange,
  MatSortable
} from "@angular/material";
import { HouseApiService } from "../services/houses-api.service";

import { LineString } from "ol/geom";
import { fromLonLat } from "ol/proj";

@Component({
  selector: "app-houses",
  templateUrl: "./houses.component.html",
  styleUrls: ["./houses.component.css"]
})
export class HousesComponent implements OnInit {
  constructor(private houseApiService: HouseApiService) {}

  listData: MatTableDataSource<any>;
  displayedColumns: string[] = ["location", "rooms", "value", "street"];
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  searchKey: string;
  filterType: string;
  sisHomeCoords: number[] = [13.4057378, 52.5418739];
  toggleRooms: boolean;
  toggleHouses: boolean;
  toggleData: boolean;
  toggleMove: boolean;
  @Input()
  id: string;

  ngOnInit() {
    // load data and prepare data for table
    this.houseApiService.getHouses().subscribe(list => {
      const array = list.map(item => {
        return {
          location: this.distanceBetweenPoints(
            [item.coords.lon, item.coords.lat],
            this.sisHomeCoords
          ),
          rooms: (item.params
          ? item.params.rooms
          : 0)
            ? item.params
              ? item.params.rooms
              : 0
            : 0,
          value: item.params ? item.params.value : 0,
          street: item.street
        };
      });

      this.listData = new MatTableDataSource(array);
      this.listData.sort = this.sort;
      this.listData.paginator = this.paginator;
      this.listData.filterPredicate = (data, filter) => {
        switch (this.filterType) {
          case "toggleRooms": // filter houses with more than 5 rooms
            return this.displayedColumns.some(ele => {
              return data["rooms"] >= Number(filter);
            });

            break;
          case "toggleData": // filter houses with incomplete data
            return this.displayedColumns.some(ele => {
              return data[ele] === Number(filter);
            });

            break;
          case "toggleMove": //  house to move to
            return this.displayedColumns.some(ele => {
              return (
                data["value"] > Number(filter) &&
                data["rooms"] >= 10 &&
                data[ele] !== 0
              );
            });
            break;
          default:
            return this.displayedColumns.some(ele => {
              return (
                data[ele]
                  .toString()
                  .toLowerCase()
                  .indexOf(filter) != -1
              );
            });
            break;
        }
      };
    });
  }

  // clear input
  onSearchClear() {
    this.searchKey = "";
    this.applyFilter();
  }

  // apply filter on input
  applyFilter() {
    this.listData.filter = this.searchKey.trim().toLowerCase();
  }

  // toggle sliders and activate filter
  toggle(event: MatSlideToggleChange) {
    switch (event.source.id) {
      case "toggleRooms":
        if (event.checked === true) {
          this.sortDataSource("rooms", "asc");
          this.filterType = "toggleRooms";
          this.listData.filter = "5";
        } else {
          this.searchKey = "";
          this.onSearchClear();
        }

        this.toggleRooms = event.checked;
        break;
      case "toggleHouses": // Close to sister house
        if (event.checked === true) {
          this.sortDataSource("location", "asc");
        } else {
          this.sortDataSource("location", "desc");
        }
        this.toggleHouses = event.checked;
        break;
      case "toggleData":
        if (event.checked) {
          this.filterType = "toggleData";
          this.listData.filter = "0";
        } else {
          this.searchKey = "";
          this.onSearchClear();
        }
        this.toggleData = event.checked;

        break;

      case "toggleMove":
        if (event.checked === true) {
          this.filterType = "toggleMove";
          this.listData.filter = "5000000";
        } else {
          this.searchKey = "";
          this.onSearchClear();
        }

        this.toggleMove = event.checked;
        break;
    }
  }

  // sort data by id
  sortDataSource(id: string, start: string) {
    this.listData.sort.sort({ id, start } as MatSortable);
    this.listData.data.sort((a: any, b: any) => {
      if (a.createdDate < b.createdDate) {
        return -1;
      } else if (a.createdDate > b.createdDate) {
        return 1;
      } else {
        return 0;
      }
    });
  }

  // calculate distance between points
  distanceBetweenPoints(startPoint: number[], endpoint: number[]) {
    const lineString = new LineString([
      fromLonLat(startPoint),
      fromLonLat(endpoint)
    ]);
    const length = lineString.getLength();
    const lineln = Math.round((length / 1000) * 100) / 100;
    return this.precisionRound(lineln, 1);
  }

  precisionRound(nbr: number, precision: number) {
    const factor = Math.pow(10, precision);
    return Math.round(nbr * factor) / factor;
  }
}
