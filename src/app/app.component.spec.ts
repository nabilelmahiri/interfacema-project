import { TestBed, async } from "@angular/core/testing";
import { AppComponent } from "./app.component";
import { MaterialModule } from "./material/material.module";
import { RouterTestingModule } from "@angular/router/testing";
describe("AppComponent", () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AppComponent],
      imports: [MaterialModule, RouterTestingModule]
    }).compileComponents();
  }));

  function setup() {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    return { fixture, app };
  }

  it(`should have as title 'interfaceMa-project'`, () => {
    const { app } = setup();
    expect(app.title).toEqual("interfaceMa-project");
  });

  it("should create the app", () => {
    const { app } = setup();
    expect(app).toBeTruthy();
  });
});
