import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { map } from "rxjs/operators";
import { House } from "../models/house";

import { Observable } from "rxjs";
@Injectable({
  providedIn: "root"
})
export class HouseApiService {
  apiURL = "/api";

  constructor(private httpClient: HttpClient) {}

  // get houses
  public getHouses(): Observable<House[]> {
    return this.httpClient
      .get(`${this.apiURL}`)
      .pipe(map((res: { houses: House[] }) => res.houses));
  }
}
