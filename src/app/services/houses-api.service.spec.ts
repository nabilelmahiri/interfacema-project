import { TestBed } from "@angular/core/testing";
import { houses } from "./data";
import {
  HttpClientTestingModule,
  HttpTestingController
} from "@angular/common/http/testing";

import { HouseApiService } from "./houses-api.service";

describe("HouseApiService", () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [HouseApiService],
      imports: [HttpClientTestingModule]
    });
    // HttpClientTestingModule - Extended interactions between a data service and the HttpClient can be complex
    // and difficult to mock with spies.
    //  The HttpClientTestingModule can make these testing scenarios more manageable.
  });

  function setup() {
    const service: HouseApiService = TestBed.get(HouseApiService);
    const httpTestingController = TestBed.get(HttpTestingController);
    return { service, httpTestingController };
  }

  it("should create HouseApiService", () => {
    const { service } = setup();
    expect(service).toBeTruthy();
  });

  it("should call the InterfaceMa data", done => {
    const { service, httpTestingController } = setup();
    const mockInterfaceMaData = houses;
    service.getHouses().subscribe(data => {
      expect(data).toEqual(mockInterfaceMaData);

      done();
    });

    const req = httpTestingController.expectOne(`${service.apiURL}`);

    expect(req.request.method).toBe("GET");

    req.flush({ houses: mockInterfaceMaData });
    httpTestingController.verify();
  });
});
